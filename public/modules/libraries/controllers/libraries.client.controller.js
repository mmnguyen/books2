'use strict';

// Libraries controller
angular.module('libraries').controller('LibrariesController', ['$scope', '$stateParams', '$location', 'Authentication',
    'Libraries', 'ngDraggable', 'activeLibraryService', '$timeout',
    function ($scope, $stateParams, $location, Authentication, Libraries, ngDraggable, activeLibraryService, $timeout) {
        $scope.authentication = Authentication;

        $scope.activeLibraryServiceObject = activeLibraryService.object;

        var errorAndRevertChanges = function (errorResponse) {
            $scope.rearrangeErrorMessage = "Error making changes to server!";

            obj.index = otherIndex;
            otherObj.index = index;

            $scope.libraries[index] = otherObj;
            $scope.libraries[otherIndex] = obj;

            return;
        };

        // Create new Library
        $scope.create = function () {
            // Create new Library object
            var library = new Libraries({
                name: $scope.libraryName
            });

            $scope.libraries.push(library);

            // Redirect after save
            library.$save(function (response) {
                // Clear form fields
                $scope.libraryName = '';
            }, function (errorResponse) {
                $scope.libraries.pop();
                $scope.addLibraryErrorMessage = "Could not save library to server!";
            });
        };

        $scope.update = function (library) {
            this.editLibrary = false;
            library.$update(function () {
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.activateTextArea = function (index) {
            var libraryTitle = document.getElementsByClassName('library-title')[index];
            var updateTextField = document.getElementsByClassName('library-update-text-field')[index];
            updateTextField.style.height = libraryTitle.offsetHeight + 3 + updateTextField.bottomPadding + 'px';
        };

        $scope.mouseClickTitle = function (index) {
            $scope.activateTextArea(index);
            var library = this;
            library.editLibrary = true;
        };

        $scope.revertChanges = function (library) {
            this.editLibrary = false;
            library.name = this.tempName;
        };

        $scope.activate = function (library, index) {
            $scope.activeLibraryServiceObject.activeLibrary = library;

            var deactiveLibraryIndex =
                $scope.activeLibraryServiceObject.activeLibraryIndex;
            $scope.activeLibraryServiceObject.activeLibraryIndex = index;

            var activeLibrary = document.getElementsByClassName('library-item')[index];
            var deactiveLibrary = document.getElementsByClassName('library-item')[deactiveLibraryIndex];

            activeLibrary.style.background = 'red';
            deactiveLibrary.style.background = '#f9f0d2';
        };

        // Remove existing Library
        $scope.remove = function (library) {
            if (library) {
                library.$remove();

                for (var i in $scope.libraries) {
                    if ($scope.libraries [i] === library) {
                        $scope.libraries.splice(i, 1);
                    }
                }
            }
        };

        // Find a search of Libraries
        $scope.find = function () {
            $scope.libraries = Libraries.query(function () {
                $scope.activeLibraryServiceObject.activeLibrary = $scope.libraries[0];
            });
        };

        // Find existing Library
        $scope.findOne = function () {
            $scope.library = Libraries.get({
                libraryId: $stateParams.libraryId
            });
        };
    }
]);
