'use strict';

//Libraries service used to communicate Libraries REST endpoints
angular.module('libraries').factory('Libraries', ['$resource',
	function($resource) {
		return $resource('libraries/:libraryId', { libraryId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

//Service to allow sharing of library data (active library) between controllers
angular.module('libraries').service('activeLibraryService', function () {
	this.object = { activeLibrary: null, activeLibraryIndex: null };
});
