'use strict';

angular.module('core').controller('SearchController', ['$scope', '$http', '$timeout', 'Authentication', 'searchService', 'activeLibraryService', 'Books',
    function ($scope, $http, $timeout, Authentication, searchService, activeLibraryService, Books) {

        $scope.authentication = Authentication;

        $scope.searchServiceObject = searchService.object;
        $scope.activeLibraryServiceObject = activeLibraryService.object;

        $scope.search = function (query) {
            if (query !== undefined && query !== '') {
                $http.get('/search/', {
                    params: {
                        q: query
                    }
                }).success(function (data) {
                    $scope.searchServiceObject.list = data.items;
                });
            }
        };

        var filterTextTimeout;
        $scope.$watch('searchServiceObject.query', function (val) {
            if (filterTextTimeout) $timeout.cancel(filterTextTimeout);
            filterTextTimeout = $timeout(function () {
                $scope.search(val);
            }, 50);
        });

        $scope.add = function (newBook) {
            $scope.searchServiceObject.list = [];
            $scope.searchServiceObject.query = '';

            newBook.library = $scope.activeLibraryServiceObject.activeLibrary._id;

            var book = new Books(newBook);

            book.$save(function(response) {

            }, function(errorResponse) {

            });
        };
    }
]);
