'use strict'

//Service to allow sharing of search data between controllers
angular.module('searches').service('searchService', function () {
    this.object = { query: '', list: [] };
});
