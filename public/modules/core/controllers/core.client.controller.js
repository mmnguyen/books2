'use strict';

angular.module('core').controller('CoreController', ['$scope', '$location', 'Authentication',
    function($scope, $location, Authentication) {
        // This provides Authentication context.
        $scope.authentication = Authentication;

        // if user is not authenticated redirect to sign in page
        if (!$scope.authentication.user || $scope.authentication.user === '') {
            $location.path('/signin');
        }
    }
]);
