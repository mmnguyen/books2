/**
 * Created by Nguyen on 02/03/2017.
 */

function getMetaTags(callback) {
    var code =  'metaTagsNL = document.getElementsByTagName("meta");' +
                'metaTags = [];' +
                'var i;' +
                'for(i = 0; i < metaTagsNL.length; i++) { metaTags.push(metaTagsNL[i].outerHTML); }' +
                '({metaTags: metaTags});';

    chrome.tabs.executeScript({
        code: code
    }, function(results) {
        var result = results[0];
        callback(result);
    });
}

function getArticle(callback) {
    var code =  'articleNL = document.getElementsByTagName("article")[0].getElementsByTagName("p");' +
                'article = [];' +
                'var i;' +
                'for(i = 0; i < articleNL.length; i++) { article.push(articleNL[i].innerHTML); }' +
                '({article: article});';

    chrome.tabs.executeScript({
        code: code
    }, function(results) {
        var result = results[0];
        callback(result);
    });
}

function renderMetaData(data) {
    var string = "";
    var i;
    for(i = 0; i < data.metaTags.length; i++) {
        string = string + data.metaTags[i] + "\n";
    }
    document.getElementById('metatags-container').textContent = string;
}

function renderArticle(data) {
    var string = "";
    var i;
    for(i = 0; i < data.article.length; i++) {
        string = string + data.article[i] + "\n";
    }
    document.getElementById('article-container').textContent = string;
}

document.addEventListener('DOMContentLoaded', function () {
    getMetaTags(function (results) {
        renderMetaData(results);
    });

    getArticle(function (results) {
        renderArticle(results);
    });
});
