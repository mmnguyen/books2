'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    passport = require('passport'),
    Schema = mongoose.Schema;

/**
 * Library Schema
 */

var CounterSchema = new Schema(
    {
        user: {
            type: Schema.ObjectId,
            ref: 'User',
            required: true
        },
        type: {
            type: String,
            required: true
        },
        seq: {
            type: Number,
            required: true
        }
    });

mongoose.model('Counter', CounterSchema);
