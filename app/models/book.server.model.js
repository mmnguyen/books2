'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    passport = require('passport'),
    Schema = mongoose.Schema;

/**
 * Book Schema
 */
var BookSchema = new Schema(
    {
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        library: {
            type: Schema.ObjectId,
            ref: 'Library'
        }
    },
    {
        strict: false
    });

mongoose.model('Book', BookSchema);
