'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    passport = require('passport'),
    Schema = mongoose.Schema;

/**
 * Library Schema
 */

var LibrarySchema = new Schema(
    {
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        name: {
            type: String,
            required: 'Please fill in a name for the library',
            trim: true
        },
        created: {
            type: Date,
            default: Date.now
        },
        index: {
            type: Number
        }
    });

mongoose.model('Library', LibrarySchema);
