'use strict';

module.exports = function(app) {
    var search = require('../../app/controllers/search.server.controller');
    var users = require('../../app/controllers/users.server.controller');

    // Libraries Routes
    app.route('/search')
        .get(users.requiresLogin, search.search);

};
