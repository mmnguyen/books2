'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	counters = require('./counters.server.controller'),
	async = require('async'),
	Library = mongoose.model('Library'),
	_ = require('lodash');

var createLibrary = function(req, seq, callback) {
	var library = new Library(req.body);
	library.user = req.user;
	library.index = seq - 1;

	library.save(function (err, library) {
		if (err) {
			callback(new Error(err));
		} else {
			callback(null, library);
		}
	});

};

/**
 * Create a Library
 */
exports.create = function(req, res) {
	async.waterfall([
		async.apply(counters.getNextSequence, req, 'library'),
		createLibrary
	], function (err, library) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if(!res._headerSent) {
				res.json(library);
			}
		}
	});
};

/**
 * Show the current Library
 */
exports.read = function(req, res) {
	res.jsonp(req.library);
};

/**
 * Update a Library
 */
exports.update = function(req, res) {
	var library = req.library ;

	library = _.extend(library , req.body);

	library.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(library);
		}
	});
};

/**
 * Delete an Library
 */
exports.delete = function(req, res) {
	var library = req.library ;

	library.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(library);
		}
	});
};

var authorization = require('../../app/controllers/users/users.authorization.server.controller'),
	passport = require('passport');
/**
 * List of Libraries for user
 */
exports.get = function(req, res) {
	Library.find(
		{
			'user': req.session.passport.user
		})
		.sort([['index', 'ascending']])
		.exec(function(err, libraries) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(libraries);
		}
	});
};

/**
 * Library middleware
 */
exports.libraryByID = function(req, res, next, id) {
	Library.findById(id).populate('user', 'displayName').exec(function(err, library) {
		if (err) return next(err);
		if (! library) return next(new Error('Failed to load Library ' + id));
		req.library = library ;
		next();
	});
};

/**
 * Library authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.library.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
