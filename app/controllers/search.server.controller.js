'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    http = require('https'),
    async = require('async'),

    Book = mongoose.model('Book'),
    User = mongoose.model('User'),

    authorization = require('../../app/controllers/users/users.authorization.server.controller');

exports.search = function (req, res) {
    var options = {
        host: 'www.googleapis.com',
        path: '/books/v1/volumes' + '?' + 'q=' + encodeURIComponent(req.query.q)
    };

    http.get(options, function (resp) {
        var body = '';
        resp.on('data', function (data) {
            return body += data;
        });

        resp.on('end', function () {
            var parsed = JSON.parse(body);
            return res.status(200).send(parsed);
        });

    }).on('error', function (err) {
        return res.status(400).send(err);
    });
};
