'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Counter = mongoose.model('Counter'),
    async = require('async'),
    _ = require('lodash'),

    Counter = mongoose.model('Counter');

exports.getNextSequence = function (req, type, callback) {
    Counter.findOneAndUpdate(
        {
            'user': req.session.passport.user,
            type: type
        },
        {
            $inc: { seq: 1 }
        },
        function (err, counter) {
            if (err) {
                callback(new Error(err));
            } else {
                callback(null, req, counter.seq);
            }
        }
    );
};

var saveCounter = function (counter, callback) {
    counter.save(function (err) {
        if (err) {
            callback(new Error(err));
        } else {
            callback(null);
        }
    });
};

/**
 * Initialize counters
 */
exports.initializeCounters = function (req, res, next) {
    var libraryCounter = new Counter({user: req.user, type: 'library', seq: 0});
    var bookCounter = new Counter({user: req.user, type: 'book', seq: 0});

    async.parallel([
        async.apply(saveCounter, libraryCounter),
        async.apply(saveCounter, bookCounter)
    ], function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            next();
        }
    });
};
