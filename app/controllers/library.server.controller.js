'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    http = require('https'),
    async = require('async'),

    Book = mongoose.model('Book'),
    User = mongoose.model('User'),

    authorization = require('../../app/controllers/users/users.authorization.server.controller');

var findBooks = function (req, user, callback) {
    if (user.library.head === null) {
        callback(null, []);
    } else {
        var books = [];
        var headID = user.library.head;
        while (headID !== null && headID !== undefined) {
            books.push(headID);
            headID = user.library.search[headID];
        }
        Book.find(
            {
                _id: {$in: books}
            }, function (err, data) {
                if (err) {
                    callback(new Error(err));
                } else {
                    callback(null, data);
                }
            }
        );
    }
};

exports.get = function (req, res) {
    async.waterfall([
        async.apply(authorization.getCurrentUser, req),
        findBooks
    ], function (err, data) {
        if (err) {
            return res.status(400).send(err);
        } else {
            return res.status(200).json(data);
        }
    });
};

var saveBook = function (book, callback) {
    book.save(function (err) {
        if (err) {
            callback(new Error(err));
        } else {
            callback(null, book);
        }
    });
};

var addBook = function (req, user, callback) {
    var book = new Book(req.body);

    if (user.library.head === null) {
        var newLibrary = {};
        newLibrary[book._id] = null;
        User.update({
                _id: user._id
            },
            {
                $set: {
                    'library.head': book._id,
                    'library.tail': book._id,
                    'library.list': newLibrary
                }
            },
            function (err, numAffected) {
                if (err) {
                    callback(new Error(err));
                } else {
                    callback(null, book);
                }
            });
    } else {
        var libraryUpdate = {};
        libraryUpdate['library.tail'] = book._id;
        libraryUpdate['library.search.' + user.library.tail] = book._id;
        libraryUpdate['library.search.' + book._id] = null;
        User.update(
            {
                _id: user._id
            },
            {
                $set: libraryUpdate
            },
            function (err, numAffected) {
                if (err) {
                    callback(new Error(err));
                } else {
                    callback(null, book);
                }
            }
        );
    }
};

exports.add = function (req, res) {
    async.waterfall([
        async.apply(authorization.getCurrentUser, req),
        addBook,
        saveBook
    ], function (err, book) {
        if (err) {
            return res.status(400).send(err);
        } else {
            return res.status(200).json(book);
        }
    });
};
